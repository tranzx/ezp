importScripts('workbox-v3.4.1/workbox-sw.js')

self.workbox.skipWaiting();
self.workbox.clientsClaim();

/*
  This is our code to handle push events.
*/
self.addEventListener('push', (event) => {
  console.log('[Service Worker] Push Received.');
  console.log(`[Service Worker] Push had this data: "${event.data.text()}"`);

  const title = 'Push Notification';
  const options = {
    body: `${event.data.text()}`,
    icon: 'images/icon.png',
    badge: 'images/badge.png'
  };

  event.waitUntil(self.registration.showNotification(title, options));
});

self.workbox.precaching.precacheAndRoute([
  {
    "url": "assets/keycloak.json",
    "revision": "137cfbb48fb7f29d697a9acc12c89048"
  },
  {
    "url": "build/app.css",
    "revision": "30962dada4a1c4eedb1ea75d3448b8c0"
  },
  {
    "url": "build/app.js",
    "revision": "1d0f9e256edf0b342a5caa76c1b4909c"
  },
  {
    "url": "build/app/3gzh0kn4.entry.js",
    "revision": "dc6647d24378673adde0c0a7b36154ac"
  },
  {
    "url": "build/app/3gzh0kn4.sc.entry.js",
    "revision": "907dde3c6d6ed2ec7a20ea461c4d17a7"
  },
  {
    "url": "build/app/app.f6eb35fv.js",
    "revision": "7fa101df97ef9d4e0f3910a8b028a557"
  },
  {
    "url": "build/app/app.iglbjppu.js",
    "revision": "2202db269f8f519c421b87631e703678"
  },
  {
    "url": "build/app/chunk-1111aee6.es5.js",
    "revision": "06a5c2a727f744aedbeeb49db49b68b3"
  },
  {
    "url": "build/app/chunk-4ef4a9f3.js",
    "revision": "3fb277c1c567471ebeb4482f12bde3f9"
  },
  {
    "url": "build/app/fd0uqf26.entry.js",
    "revision": "15dd14a85dba85724193825fbb93c173"
  },
  {
    "url": "build/app/fd0uqf26.sc.entry.js",
    "revision": "15dd14a85dba85724193825fbb93c173"
  },
  {
    "url": "build/app/fy6we7b1.entry.js",
    "revision": "158e967bb075d2ce525396b119060650"
  },
  {
    "url": "build/app/fy6we7b1.sc.entry.js",
    "revision": "970eac7637f6260be938949fd3912172"
  },
  {
    "url": "build/app/gesture.es5.js",
    "revision": "cff0f214effc4f7e083d01ddd880d7d4"
  },
  {
    "url": "build/app/gesture.js",
    "revision": "b361ab3b990fb6ca45a5bec1bb5de5fc"
  },
  {
    "url": "build/app/gjquzcq8.entry.js",
    "revision": "809a9465c43c88cad0c714c1566b866c"
  },
  {
    "url": "build/app/gjquzcq8.sc.entry.js",
    "revision": "b90c852453957772f42e6a109732c214"
  },
  {
    "url": "build/app/hardware-back-button.es5.js",
    "revision": "a6f7e1595f5f8ffa8574c2574f1a7b69"
  },
  {
    "url": "build/app/hardware-back-button.js",
    "revision": "23dd14024cf4efa75a4b9263a47f9396"
  },
  {
    "url": "build/app/input-shims.es5.js",
    "revision": "71229b623be6b5c014f79fcb3d109566"
  },
  {
    "url": "build/app/input-shims.js",
    "revision": "262e5f835e57070ac201e2ce85a083c9"
  },
  {
    "url": "build/app/ios.transition.es5.js",
    "revision": "819ce49d15ff503868b5a81dcbc3bef0"
  },
  {
    "url": "build/app/ios.transition.js",
    "revision": "4660e7f64c3bc9f73b876af9e1f6c98d"
  },
  {
    "url": "build/app/lhmnlpcm.entry.js",
    "revision": "9473272e970c6eddbd693caad3fccde1"
  },
  {
    "url": "build/app/lhmnlpcm.sc.entry.js",
    "revision": "4207fa36e0b64e437bcba86c5701a841"
  },
  {
    "url": "build/app/mcwj3ayt.entry.js",
    "revision": "3025a3dff6b2b41556834111673c46a9"
  },
  {
    "url": "build/app/mcwj3ayt.sc.entry.js",
    "revision": "afbbf25cc8e947cec0a25efeb9ec3cdd"
  },
  {
    "url": "build/app/md.transition.es5.js",
    "revision": "a6482485d53ad3d6f48350b055e1e28a"
  },
  {
    "url": "build/app/md.transition.js",
    "revision": "d85579b8d831ecdcc37d188f1c0783f7"
  },
  {
    "url": "build/app/mgtvvhly.entry.js",
    "revision": "306b6fbe1972cd5775d4224681ad6017"
  },
  {
    "url": "build/app/mgtvvhly.sc.entry.js",
    "revision": "306b6fbe1972cd5775d4224681ad6017"
  },
  {
    "url": "build/app/ntky4hp5.entry.js",
    "revision": "9d3298e213f91c36ca66b1f519f4f41f"
  },
  {
    "url": "build/app/ntky4hp5.sc.entry.js",
    "revision": "ca1dc0d410ca74a73a613421edab94a9"
  },
  {
    "url": "build/app/obdj7edg.entry.js",
    "revision": "fa86fe749ec4746f10ba481886330796"
  },
  {
    "url": "build/app/obdj7edg.sc.entry.js",
    "revision": "fa86fe749ec4746f10ba481886330796"
  },
  {
    "url": "build/app/sfa649do.entry.js",
    "revision": "b1f03ae3d74ef873909848c0bd3afd00"
  },
  {
    "url": "build/app/sfa649do.sc.entry.js",
    "revision": "00147d1e26f9200bd64d77d2ab164bfe"
  },
  {
    "url": "build/app/status-tap.es5.js",
    "revision": "1678715ca22b4d5b907f3e31b36da4a5"
  },
  {
    "url": "build/app/status-tap.js",
    "revision": "4d3c2d99f88e8b8f0507550571d3309a"
  },
  {
    "url": "build/app/svg/index.js",
    "revision": "35b1701e9c9c1dacb8be33a8bace509b"
  },
  {
    "url": "build/app/tap-click.es5.js",
    "revision": "3a425bb66d896b6050b5c93db874ec60"
  },
  {
    "url": "build/app/tap-click.js",
    "revision": "21d685ce705e580d4c112bab262b2a25"
  },
  {
    "url": "build/app/v0lfiiyf.entry.js",
    "revision": "fefd0b1622187e3cb5cc85e09d5f19f0"
  },
  {
    "url": "build/app/v0lfiiyf.sc.entry.js",
    "revision": "57261f0e5433df0eea27543748dce7b4"
  },
  {
    "url": "build/app/vvmrqlse.entry.js",
    "revision": "5559aba8bb1e68d0c4c4568d9c9e2f41"
  },
  {
    "url": "build/app/vvmrqlse.sc.entry.js",
    "revision": "5559aba8bb1e68d0c4c4568d9c9e2f41"
  },
  {
    "url": "build/app/w7nux8id.entry.js",
    "revision": "c3b36e5fc203cff8115df58c0a8c51e1"
  },
  {
    "url": "build/app/w7nux8id.sc.entry.js",
    "revision": "1ea90786878e743fec0fdce8f0b277fc"
  },
  {
    "url": "build/app/xhkbqc1l.entry.js",
    "revision": "c6190141cfb5fa5243e4388870904db2"
  },
  {
    "url": "build/app/xhkbqc1l.sc.entry.js",
    "revision": "ea4db3f36d5a76e85136a4917704e0a5"
  },
  {
    "url": "index.html",
    "revision": "0841bb7886cf913858ba6bc534015bc4"
  },
  {
    "url": "manifest.json",
    "revision": "de5ada9f7f394aad5eef19f9ba00f12a"
  }
]);